package com.jspider.maven.singleton;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class SingleClass 
{
	private static SessionFactory sessionFactory=null;
	
	private SingleClass() {
		// TODO Auto-generated constructor stub
	}
	
	
	public static SessionFactory getSessionFactory()
	{
		if(sessionFactory==null)
		{
		Configuration configuration = new Configuration();
		configuration.configure();
		SessionFactory SessionFactory1 = configuration.buildSessionFactory();
		sessionFactory=SessionFactory1;
		}
		return sessionFactory;
	}
	

}
