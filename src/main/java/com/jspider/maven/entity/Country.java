package com.jspider.maven.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspider.maven.constants.AppConstant;

@Entity
@Table(name =AppConstant.COUNTRY_INFO)
public class Country implements Serializable
{
	@Id
	@GenericGenerator(name = "c_auto", strategy = "increment")
	@GeneratedValue(generator = "c_auto")
	@Column(name = "id")
	private long id;
	
	@Column(name = "population")
	private long population;
	
//	@Column(name = "name")
//	private String name;
	
	@Column(name = "country")
	private String country;
	
	@Column(name = "continent")
	private String continent;
	
	@Column(name = "area")
	private String area;
	
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fro_key")
	private PrimeMinster primeMinster;

	public PrimeMinster getPrimeMinster() {
		return primeMinster;
	}

	public void setPrimeMinster(PrimeMinster primeMinster) {
		this.primeMinster = primeMinster;
	}

	public Country() {
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getPopulation() {
		return population;
	}

	public void setPopulation(long population) {
		this.population = population;
	}

//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}
	
	
	
	
	

}
