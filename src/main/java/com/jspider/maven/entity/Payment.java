package com.jspider.maven.entity;

import java.io.Serializable;

import java.util.Date;
import java.util.Timer;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.jspider.maven.constants.AppConstant;

@Entity
@Table(name =AppConstant.PAYMENT_INFO)

public class Payment implements Serializable
{
	
		@Id
//		@GenericGenerator(name = "p_auto", strategy ="increment")
//		@GeneratedValue(generator = "p_auto")
		
		@Column(name="id")
		private long id;
		
		@Column(name="movieid")
	   private long movieid;
		
		@Column(name="numOfTickets")
		private long numOfTickets;
		
		@Column(name="price")
		private Double price;
		
		@Column(name="showDate")
		private Date showDate;
		
		@Column(name="time")
		private Timer time;
		
		@Column(name="paymentType")
		private String paymentType;	
		
		
		@Column(name="totalprice")
		private double totalprice;
		
		public Double getTotalprice() {
			return totalprice;
		}

		public void setTotalprice(Double totalprice) {
			this.totalprice = totalprice;
		}

		public long getId() {
			return id;
		}

//		public void setId(long id) {
//			this.id = id;
//		}

		public long getMovieid() {
			return movieid;
		}

		public void setMovieid(long movieid) {
			this.movieid = movieid;
		}

		public long getNumOfTickets() {
			return numOfTickets;
		}

		public void setNumOfTickets(long numOfTickets) {
			this.numOfTickets = numOfTickets;
		}

		public Double getPrice() {
			return price;
		}

		public void setPrice(Double price) {
			this.price = price;
		}

		public Date getShowDate() {
			return showDate;
		}

		public void setShowDate(Date showDate) {
			this.showDate = showDate;
		}

		public Timer getTimer() {
			return time;
		}

		public void setTimer(Timer time) {
			this.time = time;
		}

		public String getPaymentType() {
			return paymentType;
		}

		public void setPaymentType(String paymentType) {
			this.paymentType = paymentType;
		}

	

}
