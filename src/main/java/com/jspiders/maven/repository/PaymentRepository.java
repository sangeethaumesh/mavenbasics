package com.jspiders.maven.repository;


import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.jspider.maven.entity.Payment;

public class PaymentRepository 
{
	public void savePaymentDetails(Payment payment)
	{
		try {
			
			Configuration configuration = new Configuration();
			configuration.configure();
			SessionFactory SessionFactory = configuration.buildSessionFactory();
			Session session = SessionFactory.openSession();
			Transaction Transaction = session.beginTransaction();
			session.save(payment);
			Transaction.commit();
			
			
		} 
		
		catch (Exception e) {
			// TODO: handle exception
		}
	}
	
	
}
