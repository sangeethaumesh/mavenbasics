package com.jspiders.maven.repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import com.jspider.maven.entity.Country;
import com.jspider.maven.singleton.SingleClass;

public class AssociationImpl implements AssociationClass
{
	@Override
	public void saveCountryDetails(Country country)
	{
		SessionFactory sessionFactory = SingleClass.getSessionFactory();
		Session Session = sessionFactory.openSession();
		Transaction Transaction = Session.beginTransaction();
		Session.save(country);
		Transaction.commit();
	}

}
