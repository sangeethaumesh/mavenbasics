package com.jspiders.maven.repository;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.event.spi.SaveOrUpdateEvent;
import org.hibernate.query.Query;

import com.jspider.maven.entity.Movie;



public class MovieRepository 
{
	public void saveMovieDetails(Movie movie)
	{
		
			try {
				Configuration cfg = new Configuration();
				cfg.configure();
				
			//	cfg.addAnnotatedClass(Movie.class);
				
				SessionFactory SessionFactory = cfg.buildSessionFactory();
				Session session = SessionFactory.openSession();
				Transaction transaction = session.beginTransaction();
				session.save(movie);
				transaction.commit();
				
			} catch (HibernateException e) {
				// TODO: handle exception
			}
	}
			
			
			public  Movie getMovieById(Long id)
			{
				
					Configuration cfg = new Configuration();
					cfg.configure();
					
				//	cfg.addAnnotatedClass(Movie.class);
					
					SessionFactory SessionFactory = cfg.buildSessionFactory();
					Session session = SessionFactory.openSession();
					Transaction transaction = session.beginTransaction();
					Movie m = session.get(Movie.class, id);
					transaction.commit();
					return m;
			}
			
			public Movie updateRatingAndBudgetById(String rating, Double budget, Long id) {
				
				
				Movie m=getMovieById(id);
				
				if(m!=null) {
					
					m.setRating(rating);
					m.setBudget(budget);
				}
				
				Configuration cfg = new Configuration();
				cfg.configure();
				
			//	cfg.addAnnotatedClass(Movie.class);
				
				SessionFactory SessionFactory = cfg.buildSessionFactory();
				Session session = SessionFactory.openSession();
				Transaction transaction = session.beginTransaction();
				session.saveOrUpdate(m);
				{
					System.out.println("updated successfully");
				}
				
				transaction.commit();
				return m;
			}
			
			public List<Movie> findAll()
			{
				Configuration configuration = new Configuration();
				configuration.configure();
				SessionFactory SessionFactory = configuration.buildSessionFactory();
				Session Session = SessionFactory.openSession();
				String hql="from Movie";
				Query Query = Session.createQuery(hql);
				return Query.list();
				
			}
			
			public Movie findByName(String movieName)
			{
				Configuration configuration = new Configuration();
				configuration.configure();
				SessionFactory SessionFactory = configuration.buildSessionFactory();
				Session Session = SessionFactory.openSession();
				String hql="from Movie where name=:mName";
				Query query = Session.createQuery(hql);
				query.setParameter("mName", movieName);
				 return (Movie) query.uniqueResult();
				
			}
			
			public void updateRatingAndBudgetByName(String name, String rating, Double budget)
			{
				Configuration configuration = new Configuration();
				configuration.configure();
				SessionFactory SessionFactory = configuration.buildSessionFactory();
				Session Session = SessionFactory.openSession();
				Transaction transaction = Session.beginTransaction();
				String hql="update Movie set rating=:r, Budget=:b where name=:n";
				Query query = Session.createQuery(hql);
				query.setParameter("r", rating);
				query.setParameter("b", budget);
				query.setParameter("n", name);
				int rowUpdated = query.executeUpdate();
				transaction.commit();
				if(rowUpdated>0)
				{
					System.out.println("updated successfull");
				}
				else
				{
					System.out.println("update failure");
				}
				
			}
			
			public void deleteByName(String name)
			{
				Configuration configuration = new Configuration();
				configuration.configure();
				SessionFactory SessionFactory = configuration.buildSessionFactory();
				Session Session = SessionFactory.openSession();
				Transaction transaction = Session.beginTransaction();
				String hql="delete Movie where name=:n";
				Query query = Session.createQuery(hql);
				query.setParameter("n", name);
				int rowUpdate = query.executeUpdate();
				transaction.commit();
				if(rowUpdate>0)
				{
					System.out.println("updated successfull");
				}
				else {
					System.out.println("update failure");
				}
			}
		
	}


