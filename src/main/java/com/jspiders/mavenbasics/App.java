package com.jspiders.mavenbasics;

import java.util.Date;
import java.util.List;
import java.util.Timer;

import com.jspider.maven.entity.Country;
import com.jspider.maven.entity.Movie;
import com.jspider.maven.entity.Payment;
import com.jspider.maven.entity.PrimeMinster;
import com.jspiders.maven.repository.AssociationImpl;
import com.jspiders.maven.repository.MovieRepository;
import com.jspiders.maven.repository.PaymentRepository;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Movie movie = new Movie();
        movie.setId(1L);
//        movie.setName("KGF");
//        movie.setBudget(100D);
//        movie.setRating("4");
//        movie.setReleaseDate(new Date());
        
//        MovieRepository movieRepository = new MovieRepository();
//        movieRepository.saveMovieDetails(movie);
//        
//        System.out.println( movieRepository.getMovieById(170L));
//        
//        movieRepository.updateRatingAndBudgetById("3.55", 23546D, 170L);
        
//       List<Movie> movieList= movieRepository.findAll();
//       movieList.forEach(each->{
//    	   System.out.println(each);
//       });
        
//        Movie findByName = movieRepository.findByName("beast");
//        System.out.println(findByName);
        
//       movieRepository.updateRatingAndBudgetByName("beast", "3.00", 234156D);
        
//        movieRepository.deleteByName("Beast");
        
        

//        Payment payment = new Payment();
//      
//        payment.setMovieid(movie.getId());
//        payment.setNumOfTickets(10);
//        payment.setPaymentType("UPI");
//        payment.setPrice(150D);
//		payment.setShowDate(new Date());
//		payment.setTimer(new Timer());
//		
//		payment.setTotalprice(payment.getNumOfTickets()*payment.getPrice());
//        
//		PaymentRepository paymentRepository = new PaymentRepository();
//		paymentRepository.savePaymentDetails(payment);
//		
		Country country = new Country();
		country.setCountry("india");
		country.setArea("3.28 million sq km");
		country.setPopulation(13000000L);
		country.setContinent("Asia");
		
		PrimeMinster primeMinster = new PrimeMinster();
		primeMinster.setName("modi");
		primeMinster.setAge(71L);
		primeMinster.setGender("male");
		primeMinster.setParty("BJP");
		
		country.setPrimeMinster(primeMinster);
		
		AssociationImpl associationImpl = new AssociationImpl();
		associationImpl.saveCountryDetails(country);
		
    }
}
